package mtk.util;

import java.io.*;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 * @author <a href="mailto:mtkravchenko@gmail.com">Mikhail Kravchenko</a>
 */
public class Logger {
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMMMdd_HH'h'mm'm'ss's'", Locale.US);
    public static final SimpleDateFormat logDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static int maxLogFileSize = 1024 * 1024 * 4;
    private final static Logger log = Logger.getLogger(Logger.class);
    private final static HashMap<Class<Throwable>, Logger> exceptions = new HashMap();
    private static String logRoot;
    private static File logDir;
    private static LogWriter errorPrintWriter;
    private static LogWriter allPrintWriter;

    static {
        setLogRoot(".");
    }

    private Class name;
    private LogWriter printWriter;

    protected Logger(Class clazz) {
        this.name = clazz;
    }

    public static SimpleDateFormat getDateFormat() {
        return dateFormat;
    }

    public static void setDateFormat(SimpleDateFormat dateFormat) {
        Logger.dateFormat = dateFormat;
    }

    public static int getMaxLogFileSize() {
        return maxLogFileSize;
    }

    public static void setMaxLogFileSize(int maxLogFileSize) {
        Logger.maxLogFileSize = maxLogFileSize;
    }

    public static void setLogRoot(String logRoot) {
        Logger.logRoot = logRoot;
    }

    public static Logger getLogger(Class clazz) {
        return new Logger(clazz);
    }

    protected synchronized static String getLogDir() {

        File dir = logDir;
        if (dir == null) {
            dir = new File(logRoot);
            logDir = dir;
        }
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                err("getLogDir: Can't create dir " + dir.getName());
            }
        } else if (!dir.isDirectory()) {
            return "";
        }
        return dir.getAbsolutePath() + "/";
    }

    private static void err(String s) {
        System.out.println(s);
        System.err.println(s);
    }

    public synchronized static void log(String s, Logger logger) {
        String className = logger.getClassName();
        try {
            PrintWriter printWriter = logger.getPrintWriter(); //Set true for append mode
            if (printWriter != null) {
                printWriter.println(logDateFormat.format(new Date(System.currentTimeMillis())) + ": " + s);  //New line
                logger.flush();
            }
        } catch (Exception e) {
            err(e.getMessage());
        }
        log(className + ": " + s);
    }

    public synchronized static void quickLog(String s, Logger logger) {

        try {
            PrintWriter printWriter = logger.getPrintWriter(); //Set true for append mode
            if (printWriter != null) {
                printWriter.print(s);  //New line
                logger.flush();
            }
        } catch (Exception e) {
            err(e.getMessage());
        }
    }

    private static PrintWriter getAllPrintWriter() throws IOException {
        allPrintWriter = getLogWriter(allPrintWriter, "all-log.txt", Logger.class);
        return allPrintWriter;
    }

    public synchronized static void log(String s) {

        System.out.println(s);
        try {
            PrintWriter printWriter = getAllPrintWriter();
            if (printWriter != null) {
                printWriter.println(logDateFormat.format(new Date(System.currentTimeMillis())) + " " + s);  //New line
                printWriter.flush();
            }
        } catch (IOException e) {
            err(e.getMessage());
            return;
        }
    }

    private static String getStackTrace() {
        StringBuffer sb = new StringBuffer();
        for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
            sb.append(ste.toString() + "\n");
        }
        return sb.toString();
    }

    public static String getShortErrorMessage(Throwable e) {
        if (e.getMessage() == null) {
            return getErrorMessage(e);
        } else {
            return e.getMessage();
        }
    }

    protected static String getShortErrorMessage2(Throwable e) {
        if (e.getMessage() == null) {
            return e.toString();
        } else {
            return e.getMessage();
        }
    }

    public static String getErrorMessage(Throwable e) {
        StringWriter s = new StringWriter();
        s.append(getShortErrorMessage2(e));
        if (e.getCause() != null) {
            s.append(" " + getShortErrorMessage2(e.getCause()));
        }
        s.append("\n");
        e.printStackTrace(new PrintWriter(s));
        return s.toString();
    }

    public synchronized static void error(String s, Logger logger) {
        try {
            PrintWriter printWriter = getErrorPrintWriter();
            if (printWriter != null) {
                printWriter.println(logDateFormat.format(new Date(System.currentTimeMillis())) +
                        " " + logger.getClassName() + s + "\nTrace: " + getStackTrace());  //New line
                printWriter.flush();
            }
        } catch (Exception e) {
            err(e.getMessage());
        }
    }

    private synchronized static PrintWriter getErrorPrintWriter() {
        errorPrintWriter = getLogWriter(errorPrintWriter, "all-errors.txt", Logger.class);
        return errorPrintWriter;
    }

    protected static LogWriter getLogWriter(LogWriter current, String logName, Object sync) {
        synchronized (sync) {
            File file;
            if (current != null) {
                file = current.getFile();
                current.flush();
                if (file.length() > maxLogFileSize) {
                    current.close();
                    try {
                        Files.delete(file.toPath());
                    } catch (IOException e) {
                        err("(1) Can't delete the file: " + file.getAbsolutePath() + " " + e.getMessage());
                        return current;
                    }
                    if (file.exists()) {
                        err("(2) Can't delete the file: " + file.getAbsolutePath());
                        return current;
                    }
                } else {
                    return current;
                }
            }
            file = new File(Logger.getLogDir() + logName);
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    err("file.createNewFile(): can't create" + logName + " " + e.getMessage());
                    return null;
                }
            }
            try {
                return new LogWriter(file);
            } catch (IOException e) {
                err("getPrintWriter: error while new FileWriter for " + logName + " " + e.getMessage());
                return current;
            }
        }
    }

    private PrintWriter getPrintWriter() {
        printWriter = getLogWriter(printWriter, getClassName() + ".txt", this);
        return printWriter;

    }

    private void flush() {
        if (printWriter != null) {
            printWriter.flush();
        }
    }

    public void info(String s) {
        System.out.println(s);
        log("INFO " + s, this);
    }

    public void error(String s) {
        System.err.println(s);
        error(s, this);
        log("ERROR " + s + getStackTrace(), this);
    }

    public String getClassName() {
        return name.getSimpleName();
    }

    public void info(Throwable e) {
        String m = getErrorMessage(e);
        Logger exceptionLogger = getExceptionLogger(e.getClass());
        log(m, exceptionLogger);
        String m2 = getShortErrorMessage(e);
        log(m2, this);
    }

    private Logger getExceptionLogger(Class clazz) {
        Logger exceptionLogger = exceptions.get(clazz);
        if (exceptionLogger == null) {
            exceptionLogger = getLogger(clazz);
            exceptions.put(clazz, exceptionLogger);
        }
        return exceptionLogger;
    }

    public void error(Throwable e) {
        String m = getErrorMessage(e);
        Logger exceptionLogger = getExceptionLogger(e.getClass());
        log(m, exceptionLogger);
        String m2 = getShortErrorMessage(e);
        error(m2, this);
    }

    public void error(Throwable e, String m) {
        Logger exceptionLogger = getExceptionLogger(e.getClass());
        log(m, exceptionLogger);
        String m2 = getShortErrorMessage(e);
        error(m2, this);
    }

    public static class LogWriter extends PrintWriter {
        private final File file;

        public LogWriter(File f) throws IOException {
            super(new FileWriter(f, true));
            file = f;
        }

        public File getFile() {
            return file;
        }

        @Override
        public void close() {
            super.close();

            File zip = new File(getZipName());
            ZipOutputStream out = null;
            try {
                out = new ZipOutputStream(new FileOutputStream(zip));
                out.setLevel(Deflater.BEST_COMPRESSION);
            } catch (FileNotFoundException e) {
                log.error(e);
                log.error("Can't zip " + file.getAbsolutePath());
                return;
            }
            ZipEntry entry = new ZipEntry(file.getName());
            try {
                out.putNextEntry(entry);
            } catch (IOException e) {
                log.error(e);
                log.error("Can't zip " + file.getAbsolutePath());
                return;
            }

            FileReader fr = null;
            BufferedReader reader = null;
            try {
                fr = new FileReader(file);
                reader = new BufferedReader(fr);
                for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                    out.write(line.getBytes());
                    out.write('\n');
                }
            } catch (FileNotFoundException e) {
                log.error(e.getMessage());
                log.error("Can't zip " + file.getAbsolutePath());
                return;
            } catch (IOException e) {
                log.error(e.getMessage());
                log.error("Can't zip " + file.getAbsolutePath());
                return;
            } finally {
                try {
                    if (out != null) {
                        out.closeEntry();
                    }
                } catch (IOException e) {
                }
                try {
                    if (out != null) {
                        out.finish();

                    }
                } catch (IOException e) {
                }

                try {
                    if (out != null) {
                        out.close();

                    }
                } catch (IOException e) {
                }

                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                }
                try {
                    if (fr != null) {
                        fr.close();
                    }
                } catch (IOException e) {
                }
            }
        }

        private String getZipName() {
            String s = file.getName().replace(".txt",
                    "_" + dateFormat.format(new Date(System.currentTimeMillis())) +
                            ".zip");

            return file.getParent() + "/" + s;
        }
    }

}
